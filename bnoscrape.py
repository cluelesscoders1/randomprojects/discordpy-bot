import os
import requests
import flag
import json 

bnoColumns = [
	'cases',
	'newcases',
	'deaths',
	'newdeaths',
	'deathrate',
	'sercrit',
	'recovered'
]

bnoCountry = [
	'cases',
	'deaths',
	'recovered',
	'unresolved'
]

bnoIntTotal = [
	'case',
	'deaths', 
	'recovered',
	'unresolved'
]

bnoIntColumns = [
	'cases',
	'newcases',
	'deaths',
	'newdeaths',
	'percdeath',
	'sercrit',
	'recovered',
	'blank0',
	'blank1',
	'cases',
	'deaths'
]

#### </BNOScrape> #### 
def BnoOutput(bnoout):
	"""Turns output into a string"""
	retval = str()
	realstate = 'USA'
	if len(bnoout.keys()) > 5:
		realstate = bnoout['state'] 

	retval += realstate + " Covid-19 statistics:\n"
	for inkey in bnoout.keys():
		if inkey != 'state':
			retval += f"{inkey}: {bnoout[inkey]} -- ".title()

	return retval[:-4]

def BnoIntOutput(bnoout):
	"""Turns output into a string"""
	retval = str()
	realcountry = 'WorldWide'
	if len(bnoout.keys()) > 7:
		realcountry = bnoout['country'] 

	retval += realcountry + " Covid-19 statistics:\n "
	for inkey in bnoout.keys():
		if inkey != 'country':
			retval += f"{inkey}: {bnoout[inkey]} -- ".title()

	return retval[:-4]

def BnoState(datastr, state=None):
	pageStr = datastr
	giveUsStats = False
	giveTop5 = False
	t5retval = ''
	retval = ''
	localindex = 0
	cutstart = 0
	endChar = '<'
	markerText = "dir=\"ltr\">"
	markerLen = len(markerText)
	retvalState = {}
	statsDepth = 7
	if state == 'Top5':
		giveTop5 = True
	elif state == None or state == '':
		giveUsStats = True
	else: 
		try:
			if not giveUsStats and not giveTop5:
				localindex = pageStr.index(state)
				pageStr=pageStr[localindex+16:]
				for column in range(statsDepth):
					tmpStr = ''
					localindexMarker = pageStr.index(markerText) + markerLen
					localindexChar = pageStr.index('\">') + 2
					localindex = localindexMarker if localindexMarker < localindexChar else localindexChar
					currentChar = pageStr[localindex]
					while currentChar != endChar:
						tmpStr += currentChar 
						localindex += 1
						currentChar = pageStr[localindex]
					if tmpStr == '':
						tmpStr = '0'
					localindex+=5
					pageStr = pageStr[localindex:]
					retvalState[bnoColumns[column]] = tmpStr 
					# print(f"{ bnoColumns[column] } : {tmpStr}")
				retvalState['state'] = state
			else:
				'''if Top5 selected'''
				for topFive in range(5):
					if (topFive % 2) == 0:
						markerText = "td class=\"s15\""
					else:
						markerText = "td class=\"s22\""
					markerIndex = pageStr.index(markerText)
					pageStr = pageStr[markerIndex + 25:]
					state = pageStr[:pageStr.index(endChar)]
					localindex = pageStr.index(state)
					pageStr = pageStr[localindex+16:]
					for column in range(statsDepth):
						tmpStr = ''
						localindexMarker = pageStr.index(markerText) + markerLen
						localindexChar = pageStr.index('\">') + 2
						localindex = localindexMarker if localindexMarker < localindexChar else localindexChar
						currentChar = pageStr[localindex]
						while currentChar != endChar:
							tmpStr += currentChar
							localindex += 1
							currentChar = pageStr[localindex]
						if tmpStr == '':
							tmpStr = '0'
						localindex += 5
						pageStr = pageStr[localindex:]
						retvalState[bnoColumns[column]] = tmpStr
						# print(f"{ bnoColumns[column] } : {tmpStr}")
					retvalState['state'] = state
					cutstart = len(state) + 22
					t5val = (BnoOutput(retvalState)) + "\n"
					t5retval += state + " " + t5val[cutstart:]
				return t5retval

		except ValueError as e:
			giveUsStats = True
			pass
		except Exception as e:
			raise e 

	if giveUsStats:
		retvalCountry = {}
		try: 
			# Initial index
			initialMark = 'RECOVERED'
			secondaryMark = '</th>'
			tripleMarker = '\">'
			endChar = '<'
			localindex = 0 

			# Get into position	
			localindex = pageStr.index(initialMark)
			pageStr = pageStr[localindex:]
			localindex = pageStr.index(secondaryMark)
			pageStr = pageStr[localindex:]
			for column in range(4):
				tmpStr = ''
				localindex = pageStr.index(tripleMarker) + len(tripleMarker)
				currentChar =  pageStr[localindex]
				while currentChar != endChar:
					tmpStr += currentChar
					localindex += 1
					currentChar = pageStr[localindex]
				if tmpStr == '':
					tmpStr = '0'
				localindex+=5
				pageStr = pageStr[localindex:]
				retvalCountry[bnoCountry[column]]=tmpStr
		except: 
			return str(e)
		finally:
			return retvalCountry
	return retvalState

def ScrapeBNO(state=None):
	"""Scrapes the BNO webpage for stats by state."""
	link = "https://docs.google.com/spreadsheets/d/e/2PACX-1vR30F8lYP3jG7YOq8es0PBpJIE5yvRVZffOyaqC0GgMBN6yt0Q-NI8pxS7hd1F9dYXnowSC6zpZmW9D/pubhtml/sheet?headers=false&gid=1902046093"
	request = requests.get(link)
	pageStr = str(request.text)

	lastIndex = 0
	endChar = '<'
	valuesFound = []
	localindex = 0
	if state != "Top5":
		bnoreport = BnoState(pageStr, state)
		return BnoOutput(bnoreport)
	else:
		return BnoState(pageStr, state)

def BnoInt(datastr,country):
	pageStr = datastr
	giveUsStats = False
	retval = ''
	localindex = 0
	endChar = '<'
	markerText = "dir=\"ltr\">"
	markerLen = len(markerText)
	retvalCountry = {}
	if country == None or country == '':
		giveUsStats = True
	else: 
		try:
			if not giveUsStats:
				localindex = pageStr.index('Links')
				pageStr=pageStr[localindex+16:]
				localindex = pageStr.index(country) # Repeat to not get stuck on a tab at the top
				pageStr=pageStr[localindex+5:]
				for column in range(7):
					tmpStr = ''
					localindexMarker = pageStr.index(markerText) + markerLen
					localindexChar = pageStr.index('\">') + 2
					localindex = localindexMarker if localindexMarker < localindexChar else localindexChar
					currentChar = pageStr[localindex]
					while currentChar != endChar:
						tmpStr += currentChar 
						localindex += 1
						currentChar = pageStr[localindex]
					if tmpStr == '':
						tmpStr = '0'
					localindex+=5
					pageStr = pageStr[localindex:]
					retvalCountry[ bnoIntColumns[column] ] = tmpStr 
					# print(f"{ bnoIntColumns[column] } : {tmpStr}")
				retvalCountry['country'] = country 

		except ValueError as e:
			giveUsStats = True
			pass
		except Exception as e:
			raise e 

	if giveUsStats:
		retvalCountry = {}
		try: 
			# Initial index
			initialMark = 'RECOVERED'
			secondaryMark = '</th>'
			tripleMarker = '\">'
			endChar = '<'
			localindex = 0 

			# Get into position	
			localindex = pageStr.index(initialMark)
			pageStr = pageStr[localindex:]
			localindex = pageStr.index(secondaryMark) 
			pageStr = pageStr[localindex:]
			for column in range(4):
				tmpStr = ''
				localindex = pageStr.index(tripleMarker) + len(tripleMarker)
				currentChar =  pageStr[localindex]
				while currentChar != endChar:
					tmpStr += currentChar
					localindex += 1
					currentChar = pageStr[localindex]
				if tmpStr == '':
					tmpStr = '0'
				localindex+=5
				pageStr = pageStr[localindex:]
				retvalCountry[bnoCountry[column]]=tmpStr
		except: 
			return str(e)
		finally:
			return retvalCountry
	return retvalCountry

def ScrapeBnoInt(country=None):
	"""Scrapes BNO international Google Sheet"""
	link = "https://docs.google.com/spreadsheets/d/e/2PACX-1vR30F8lYP3jG7YOq8es0PBpJIE5yvRVZffOyaqC0GgMBN6yt0Q-NI8pxS7hd1F9dYXnowSC6zpZmW9D/pubhtml?gid=0&amp;single=true&amp;widget=true&amp;headers=false&amp;range=A1:I193"
	request = requests.get(link)
	pageStr = str(request.text)

	lastIndex = 0
	endChar = '<'
	valuesFound = []
	localindex = 0

	bnoReport = BnoInt(pageStr,country)
	return BnoIntOutput(bnoReport)
	# return bnoReport

#### </BNOScrape> #### 