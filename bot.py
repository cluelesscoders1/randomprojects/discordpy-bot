import os
import discord
import requests
import flag
import json 
import re
import locale
from bnoscrape import * 
# from bing import *
from coronalmao import *

TOKEN = os.getenv('DISCORD_TOKEN')


statejson = {}
with open("states.json",'r') as statefile:
	statejson = json.load(statefile)

def GetLatestUs():
	"""Gets the latest Covid-19 US summary"""
	link = "https://covidtracking.com/api/us"
	page = requests.get(link)
	pagejson = page.json() 
	retval = f"Positive: {pagejson[0]['positive']}  hospitalized: {pagejson[0]['hospitalized']} Deaths: {pagejson[0]['death']}"
	return retval 

def ScrapeCovid():
	"""Scrapes https://coronavirus.1point3acres.com/en for latest information."""

	link = "https://coronavirus.1point3acres.com/en" 
	page = requests.get(link)
	pageStr = str(page.data)

	indexMarker = "<strong class=\"jsx-1633900136\">"
	lastIndex = 0
	endChar = '<'
	valuesFound = []
	localindex = 0

	for _ in range(6): # Number of values to pluck
		localindex =  pageStr.index(indexMarker) + len(indexMarker)
		currentChar = pageStr[localindex]
		tmpStr = ''
		while currentChar != endChar:
			tmpStr += currentChar
			localindex += 1 
			currentChar = pageStr[localindex] 
		localindex += 1
		pageStr = pageStr[localindex:]
		valuesFound.append(tmpStr)

	# print("Values found")
	# for myval in valuesFound:
	#	 print(f"{myval}")
	retstr = f"US cases: {valuesFound[0]} US recov: {valuesFound[1]} US dead: {valuesFound[2]}\n"
	retstr += f"CA cases: {valuesFound[3]} CA recov: {valuesFound[4]} CA dead: {valuesFound[5]}"
	return retstr

def main():
	"""Executed function"""
	# Info/constnats 
	helpcommands = "Commands:\n"
	helpcommands += "covidus: covidus API response from https://covidtracking.com/api/us\n"
	# helpcommands += "covidam: scraped results from https://coronavirus.1point3acres.com/en"
	helpcommands += "bnous: argument of state name or blank for USA updated stats from BNO\n"
	helpcommands += "bnoint: argument of country or blank for WorldWide updated stats from BNO\n"
	# helpcommands += "bapius  --Returns USA statistics updated from Bing tracker\n"
	# helpcommands += "bapius <XX>  --Returns US State statistics using 2-letter abbreviation\n"
	# helpcommands += "bapiint  --Returns Global total statistics updated from Bing tracker\n"
	# helpcommands += "bapiint <countryname>  --Returns specific Country statistics using full country name"
	helpcommands += "lmaous: State with optional county stats from corona.lmao.ninja\n"
	helpcommands += "lmaoint: International stats from corona.lmao.ninja\n"
	helpcommands += "lmaous or lmaoint: Top20 will produce top 20 list\n"
	helpcommands += "lmaous or lmaoing Selk20: Produce test list of top 20"

	# Bot activity
	client = discord.Client()

	bnopattern = re.compile("bnous")
	bnoIntPattern = re.compile("bnoint")
	bingUSPattern = re.compile("bapius")
	bingIntPattern = re.compile("bapiint")
	ncUSPattern = re.compile("lmaous")
	ncIntPattern = re.compile("lmaoint")

	# bingLookup = input("Bing Covid: ").lower()

	@client.event
	async def on_ready():
		print(f'{client.user} has connected to Discord!')

	@client.event
	async def on_message(message):
		if message.content.lower() == 'helpmedago':
			await message.channel.send(helpcommands) 
		elif message.content.lower() == 'covidus':
			await message.channel.send(str(GetLatestUs()))
		# elif message.content.lower() == 'covidam':
		# 	await message.channel.send(ScrapeCovid())

		###### <BNO> ######
		elif bnopattern.match(message.content.lower()):
			state = message.content[6:]
			'''check for abbreviation and convert'''
			try: 
				if len(state) <= 3: # just incase there is a 
					state = statejson['states'][(state[:2].upper())]
			except: 
				print(f"2 letter: {state} not found.")
				state = None
			if state == ' ' or state == '': 
				state = None
			print(f"state: {state}")
			await message.channel.send(ScrapeBNO(state))

		elif bnoIntPattern.match(message.content.lower()):
			country = message.content[7:]
			if country == ' ' or '' or None:
				country = None
			print(f"country: {country}")
			await message.channel.send(ScrapeBnoInt(country))
		###### </BNO> ######
	
		###### <BING> ######
		# elif bingUSPattern.match(message.content.lower()):
		# 	getState = message.content[7:]
		# 	if getState == '':
		# 		print("Country: unitedstates")
		# 		await message.channel.send(countryStats("unitedstates"))
		# 	elif " ".join(getState.split()).count(" ") > 0:
		# 		getCounty = " ".join(getState.split()).split()[1]
		# 		print(f"State: {getState} - County: {getCounty}")
		# 		await message.channel.send(usCountyStats(getState, getCounty))
		# 	else:
		# 		print(" ".join(getState.split()).count(" "))
		# 		print(f"State: {getState}")
		# 		await message.channel.send(usStateStats(getState))
		
		# elif bingIntPattern.match(message.content.lower()):
		# 	getCountry = message.content[8:].lower().replace(" ", "")
		# 	print(f"Country: {getCountry}")
		# 	await message.channel.send(countryStats(getCountry))
		###### </BING> ######


		###### <lmao> ######
		elif ncUSPattern.match(message.content.lower()):
			getState = message.content[7:]
			if getState == '':
				print("Country: United States")
				await message.channel.send(countryStats("United States"))
			elif getState.lower().startswith("top"):
				print(f"Top {getState[3:]} states.")
				await message.channel.send(getTopX("States",getState[3:]))
			elif getState.lower().startswith("selk"):
				print(f"Top {getState[4:]} states.")
				await message.channel.send(SelkieSmooth("States",getState[4:]))
			elif len(getState) > 2 and len(getState.split()[0]) == 2:
				getCounty = getState[3:]
				print(f"State: {getState} - County: {getCounty}")
				await message.channel.send(usCountyStats(getState, getCounty))
			else:
				print(f"State: {getState}")
				await message.channel.send(usStateStats(getState))
		
		elif ncIntPattern.match(message.content.lower()):
			getCountry = message.content[8:].lower()
			if getCountry.lower().startswith("top"):
				print(f"Top {getCountry[3:]} countries.")
				await message.channel.send(getTopX("Countries", getCountry[3:]))
			if getCountry.lower().startswith("selk"):
				print(f"Top {getCountry[4:]} countries.")
				await message.channel.send(SelkieSmooth("Countries", getCountry[4:]))
			else:
				print(f"Country: {getCountry}")
				await message.channel.send(countryStats(getCountry))
		###### </lmao> ######

	client.run(TOKEN)

if __name__ == '__main__':
	main()
